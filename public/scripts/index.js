Vue.component('interval-row', {
	props: ['interval'],
	template: ' <tr>\n\
					<td>{{interval.date_start}}</td>\n\
					<td>{{interval.date_end}}</td>\n\
					<td>{{interval.price}}</td>\n\
					<td><input type="checkbox" :checked="interval.mon == 1" disabled="true" /></td>\n\
					<td><input type="checkbox" :checked="interval.tue == 1" disabled="true" /></td>\n\
					<td><input type="checkbox" :checked="interval.wed == 1" disabled="true" /></td>\n\
					<td><input type="checkbox" :checked="interval.thu == 1" disabled="true" /></td>\n\
					<td><input type="checkbox" :checked="interval.fri == 1" disabled="true" /></td>\n\
					<td><input type="checkbox" :checked="interval.sat == 1" disabled="true" /></td>\n\
					<td><input type="checkbox" :checked="interval.sun == 1" disabled="true" /></td>\n\
				</tr>'
});

var data = {
	intervals: [],
	newInterval: {}
}

new Vue({
	el: '#app',
	data: data,
	mounted: function() {
		this.getIntervalList();
	},
	methods: {
		getIntervalList: function(){
			const vm = this;
			fetch('/api/intervals')
				.then(function(response){
					if(response.status === 200) {
						return response.json();
					}
					alert('not 200');
				})
				.then(function(res){
					vm.intervals = res;
				})
				.catch(function(err){alert(err); console.log(err);})
		}
	}
});
