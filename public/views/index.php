<!DOCTYPE html>
<html>
    <head>
        <title>MAIN PAGE</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<link rel="stylesheet" href="css/index.css" />
    </head>
    <body>
		<div class="container-fluid" id="app">
			<!--Navigation-->
			<div class="row">
				<div class="col-sm">
					<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
						  <span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarNav">
						  <ul class="navbar-nav">
							<li class="nav-item active">
							  <a class="nav-link" href="/">Interval list<span class="sr-only">(current)</span></a>
							</li>
							<li class="nav-item">
							  <a class="nav-link" href="/add">Add interval</a>
							</li>
							<li class="nav-item">
							  <a class="nav-link" href="#">Preview maybe</a>
							</li>
						  </ul>
						</div>
					</nav>
				</div>
			</div>
			<!--Content-->
			<div class="row" v-if="intervals.length > 0">
				<div class="col-sm">
					<table class="table">
						<thead>
						  <tr>
							<th scope="col">Start</th>
							<th scope="col">End</th>
							<th scope="col">Price</th>
							<th scope="col">Mon</th>
							<th scope="col">Tue</th>
							<th scope="col">Wed</th>
							<th scope="col">Thu</th>
							<th scope="col">Fri</th>
							<th scope="col">Sat</th>
							<th scope="col">Sun</th>
						  </tr>
						</thead>
						<tbody>
							<tr 
								is="interval-row"
								v-for="interval in intervals"
								v-bind:interval="interval"
								v-bind:key="'internal-row_' + interval.id"></tr>
						</tbody>
					  </table>
				</div>
			</div>
		</div>
	</div>
    </body>
	<script type="text/javascript" src="scripts/index.js"></script>
</html>
