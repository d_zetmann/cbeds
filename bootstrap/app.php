<?php
declare(strict_types=1);
ini_set('display_errors', 'on');
error_reporting(E_ALL);

function dd($var) {
	var_dump($var);
	die;
}

require __DIR__ . '/../vendor/autoload.php';

use Libraries\Utils\Config;
use Libraries\Utils\DB;

//@todo routes to separate file
// register routes
$router = new Http\Router();
$router->addRoute('GET', '/', '\Http\Controllers\HomeController', 'index');
//$router->addRoute('GET', '/test', '\Http\Controllers\HomeController', 'get');
$router->addRoute('GET', '/shmet', '\Http\Controllers\HomeController', 'shmet');
$router->addRoute('GET', '/interval/create', '\Http\Controllers\Api\IntervalController', 'create');
$router->addRoute('GET', '/interval', '\Http\Controllers\Api\IntervalController', 'index');
$router->addRoute('GET', '/test', '\Http\Controllers\Api\IntervalController', 'test');
$router->addRoute('GET', '/delete', '\Http\Controllers\Api\IntervalController', 'delete');
$router->addRoute('GET', '/check', '\Http\Controllers\Api\IntervalController', 'checkValid');
$router->addRoute('GET', '/fetch', '\Http\Controllers\Api\IntervalController', 'fetch');

$router->addRoute('GET', '/api/intervals', '\Http\Controllers\Api\IntervalController', 'index');

// set config
//putenv("ENV=PRODUCTION");
putenv("ENV=DEVELOPMENT");

Config::set('ROOTDIR', realpath($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . '../'));
Config::set('VIEWSDIR', Config::get('ROOTDIR') . DIRECTORY_SEPARATOR . 'public/views');
Config::set('TEMPLATE_404', '404.php');
Config::set('TEMPLATE_500', '500.php');

Config::set('DBHOST', 'localhost');
Config::set('DBNAME', 'cbeds');
Config::set('DBUSER', 'root');
Config::set('DBPASS', 'root');

Config::set('CONTROLLER_DI_REPO', [
	'\Http\Controllers\Api\IntervalController' => '\Libraries\Repository\IntervalRepo'
]);

DB::getInstance();

$app = new Application($router);