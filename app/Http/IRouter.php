<?php
namespace Http;

/**
 *
 * @author zetmann
 */
interface IRouter {

	public function addRoute(string $HTTPMethod, string $URI, string $controller, string $method);
	
	public function getRoute() : array;

}
