<?php

namespace Http\Controllers\Api;
use Libraries\Utils\Config;


/**
 * Description of Controller
 *
 * @author dpog
 */
abstract class Controller {
	
	protected $request;
	
	public function __construct(\Http\IRequest $request) {
		$this->request = $request;
		$this->injectRepo();
	}
	
	private function injectRepo() {
		$reflClass = new \ReflectionClass($this);
		$classname = '\\' . $reflClass->getName();
		
		$repoConf = Config::get('CONTROLLER_DI_REPO');
		if ($repoConf && array_key_exists($classname, $repoConf)) {
			if (class_exists($repoConf[$classname])) {
				$this->repo = new $repoConf[$classname];
			} else {
				throw new \Exception($repoConf[$classname] . 'not found');
			}

		}
	}
}
