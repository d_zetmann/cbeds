<?php

namespace Http\Controllers\Api;

use Libraries\Interval\Processor;
use Http\Response;

/**
 * Description of IntervalController
 *
 * @author dpog
 */
class IntervalController extends Controller {
	
	public function index() {
		$ints = $this->repo->getAll();
		
//		$d1 = new \DateTime($ints[0]['date_start']);
//		$d2 = new \DateTime($ints[0]['date_end']);
//		$p = new Processor($ints);
//		$p->format();
		Response::json($ints);
	}
	
	
	public function create() {
		$data = [
			'date_start' => '2018-05-24',
			'date_end' => '2018-06-27',
			'price' => '5',
			'mon' => '1',
			'tue' => '0',
			'wed' => '1',
			'thu' => '0',
			'fri' => '1',
			'sat' => '0',
			'sun' => '1'
		];
		dd($this->repo->addInterval($data));
	}
	
	public function test(){
//		$data = [
//			'date_start' => '2018-05-24',
//			'date_end' => '2018-06-27',
//			'price' => '5',
//			'mon' => '1',
//			'tue' => '0',
//			'wed' => '1',
//			'thu' => '0',
//			'fri' => '1',
//			'sat' => '0',
//			'sun' => '1'
//		];
		$data = [
			'date_start' => '2018-08-20',
			'date_end' => '2018-08-25',
			'price' => '5',
			'mon' => '1',
			'tue' => '0',
			'wed' => '1',
			'thu' => '0',
			'fri' => '1',
			'sat' => '0',
			'sun' => '1'
		];
		return $this->repo->insert($data);
	}
	
	public function delete() {
		$data = [
			'date_start' => '2018-05-24',
			'date_end' => '2018-06-27',
			'price' => '5',
			'mon' => '1',
			'tue' => '0',
			'wed' => '1',
			'thu' => '0',
			'fri' => '1',
			'sat' => '0',
			'sun' => '1'
		];
		
		return $this->repo->deleteByParams($data);
	}
	
	public function checkValid() {
		$data = [
			'date_start' => '2018-05-24',
			'date_end' => '2018-06-27',
			'price' => '5100',
			'mon' => '1',
			'tue' => '0',
			'wed' => '1',
			'thu' => '0',
			'fri' => '1',
			'sat' => '0',
			'sun' => '1'
		];
		if ($this->repo->checkInvalid($data)) {
			return Response::json(['NOT valid'], 400);
		}
		
		$this->repo->insert($data);
	}
	
	public function fetch() {
		$data = [
			'date_start' => '2018-05-24',
			'date_end' => '2018-06-27',
			'price' => '5',
			'mon' => '1',
			'tue' => '0',
			'wed' => '1',
			'thu' => '0',
			'fri' => '1',
			'sat' => '0',
			'sun' => '1'
		];
		
//		return $this->repo->fetchIntersecting($data);
		return Response::json($this->repo->fetchIntersecting($data), 200);
	}
}

//'2018-04-24', '2018-04-27', '7', '1', '0', '1', '0', '1', '0', '1'