<?php

namespace Http\Controllers;

use Http\Response;
use Libraries\Utils\DB;
use Libraries\Repository\Test;

/**
 * Description of HomeController
 *
 * @author zetmann
 */
class HomeController {
	
	
	public function index () {
		return Response::html('index.php');
	}
	
	public function get () {
		$output = [];
		$conn = DB::getInstance()->getConnection();
		$res = $conn->query('SELECT * FROM test');
		while($row = $res->fetch_assoc()) {
			array_push($output, $row);
		}
		
		dd($output);
		dd('GET from HOME CONTROLLER');
	}
	
	public function shmet() {
		// do we need models at all? 
		// DI in Controller.php with repo to call this->repo
		$repo = new Test();
		
		dd($repo->getAll());
	}
}
