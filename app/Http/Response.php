<?php

namespace Http;

use Libraries\Utils\Config;
use Exceptions\NotFoundException;

/**
 * Class that handles responses: JSON and HTML and HTML404 are available
 */
class Response {
	
	/**
	 * Renders JSON response
	 * 
	 * @param array $data
	 * @param intger $code
	 */
	public static function json($data, $code = 200)
	{
		$output = $data;
		header('Content-Type: application/json', true, $code);
		
		if (is_array($data))
		{
			$output = json_encode($data);
		}
		
		echo $output;
		exit;
	}
	
	/**
	 * Renders HTML response
	 * 
	 * @param string $template
	 */
	public static function html($template, $code = 200)
	{
		$file = Config::get('VIEWSDIR'). DIRECTORY_SEPARATOR . $template;

		if (file_exists($file))
		{
			header('Content-Type: text/html', true, $code);
			include $file;
			exit;
		}
		
		return self::html404();
	}
	
	/**
	 * Fallback for 404, if template is not found
	 */
	public static function html404() {
		header('Content-Type: text/html', true, 404);
		echo '<h1>Resource not found</h1>';
		exit;
	}
}
