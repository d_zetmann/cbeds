<?php

namespace Http;

/**
 *
 * @author dpog
 */
interface IRequest {
	
	public function get($key);
}
