<?php

namespace Http;

/**
 * Description of Request
 *
 * @author dpog
 */
class Request implements IRequest {
	
	private $data = [];
	
	public function __construct() {
		$this->data = $_REQUEST; // @todo sanitize all data with filter_input() or similar
	}
	
	public function get($key) {
		if (array_key_exists($key, $this->data)) {
			return $this->data[$key];
		}
		
		return null;
	}
}
