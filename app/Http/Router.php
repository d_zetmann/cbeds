<?php

namespace Http;

use Exceptions\NotFoundException;
//@TODO
// add type hints
// add function return type

class Router implements IRouter {
	private $routes = [];

	public function addRoute(string $HTTPMethod, string $URI, string $controller, string $method){
		array_push($this->routes, [$HTTPMethod, $URI, $controller, $method]);
	}
	
	public function getRoute() : array {
		$method = $_SERVER['REQUEST_METHOD'];
		$parsedUri = parse_url($_SERVER['REQUEST_URI']);
		$URI = rtrim($parsedUri['path'], '/');
		
		if ($URI === '') {
			$URI = '/'; // frontpage
		}
		
		foreach ($this->routes as $r) {
			if ($r[1] === $URI && $r[0] === $method) {
				return ['class' => $r[2], 'method' => $r[3]];
			}
		}
		
		throw new NotFoundException();
	}
}