<?php

namespace Libraries\Repository;

use Libraries\Repository\BaseRepo;

/**
 * Description of TestRepo
 *
 * @author dpog
 */
class Test extends BaseRepo {
	protected $table = 'test';
}
