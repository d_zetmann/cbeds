<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Libraries\Repository;

use Libraries\Utils\DB;
use Exceptions\DBException;

/**
 * Description of BaseRepo
 *
 * @author dpog
 */
abstract class BaseRepo {
	// @todo limit offset
	protected $table;
	protected $fields;
	protected $connection;
	
	public function __construct() {
		if (getenv('ENV') === 'TESTING') {
			$this->connection = DB::getInstance()->getTestConnection();
			return;
		}
		
		$this->connection = DB::getInstance()->getConnection();
	}
	
	public function getAll() {
		$sql = "SELECT * FROM {$this->table}";
		
		$res = $this->connection->query($sql);
		
		return $this->getResponse($res);
	}
	
	public function getByParams(Array $params) {
		$sql = $this->whereParams($params);
		$sql = "SELECT * FROM {$this->table} WHERE {$sql}";
		
		$res = $this->query($sql);
		
		return $this->getResponse($res);
	}
	
	public function insert(Array $params) {
		$fields = '(' . implode(',', array_keys($params)) . ')';
		$values = '("' . implode('","', array_values($params)) . '")';
		
		$sql = "INSERT INTO {$this->table} {$fields} VALUES {$values};";
		$res = $this->query($sql);
		
		return $res;
	}
	
	public function deleteByParams(Array $params) {
		$sql = $this->whereParams($params);
		$sql = "DELETE FROM {$this->table} WHERE {$sql}";
		
		return $this->query($sql);
	}
	
	protected function getResponse($res) {
		$output = [];
		
		while($row = $res->fetch_assoc()) {
			array_push($output, $row);
		}
		
		return $output;
	}
	
	protected function query($sql) {
		$res = $this->connection->query($sql);

		if (!$res) {
			throw new DBException(mysqli_error($this->connection));
		}
		
		return $res;
	}
	
	protected function whereParams(Array $params) {
		$sql = '';
		$and = 'AND ';
		foreach ($params as $k => $v) {
			$sql .= $k . '="' . $v . '" ' . $and; 
		}
		
		return rtrim($sql, $and);
	}
}
