<?php

namespace Libraries\Repository;

use Libraries\Repository\BaseRepo;
use Libraries\Interval\Processor;

/**
 * Description of IntervalRepo
 *
 * @author dpog
 */
class IntervalRepo extends BaseRepo {
	protected $table = 'intervals';
	
	protected $fields = [
		'date_start',
		'date_end',
		'price',
		'mon',
		'tue',
		'wed',
		'thu',
		'fri',
		'sat',
		'sun'
	];
	
	public function addInterval($fields) {
		$set = [];
		$values = [];
		foreach ($fields as $k => $v) {
			if (in_array($k, $this->fields)) {
				array_push($set, $k);
				array_push($values, '"'.$v.'"');
			}
		}
		$set = implode(',', $set);
		$values = implode(',', $values);
		$sql = "INSERT INTO {$this->table} ({$set}) VALUES ({$values});";

		return $this->query($sql);
	}
	
	/*public function insert(Array $data) {
		// if true we merge intervals
		if ($overlapped = $this->checkOverlapped($data)) {
			$processor = new Processor;
			$data = $processor->mergeInterval($overlapped, $data);
			// @todo delete old merge and write new
			// or update current - only one request
//			dd('here');
			dd($overlapped);
		}
			dd(parent::insert($data));
	}*/
	public function insert(Array $data) {
		// if true we merge intervals
		if ($intersected = $this->fetchIntersecting($data)) {
			$processor = new Processor;
			$data = $processor->mergeInterval($intersected, $data);
			// @todo delete old merge and write new
			// or update current - only one request
//			dd('here');
			dd($intersected);
		}
		dd('done');
			dd(parent::insert($data));
	}
	
	public function checkOverlapped(Array $data) {
		$sql = $this->getOverlapParams($data);
		$res = $this->query($sql);
		// if more than one returned, think it over
		if ($overlapped) {
			return $overlapped;
		}
		
		return null;
	}
	
	private function getOverlapParams(Array $params) {
		// end date within interval
		$res = "(({$this->getEndDateWithin($params['date_end'])})";
		// or start date within interval
		$res .= " OR ({$this->getStartDateWithin($params['date_start'])}))";
		// and the same price
		$res .= " AND price = {$params['price']}";
		
		return "SELECT * FROM {$this->table} WHERE {$res}";
	}
	
	/**
	 * Fetching here intervals that can intersect and could be merged with incoming
	 * 
	 * @param array $params
	 * @return array
	 */
	public function fetchIntersecting(Array $params) {
		// end date within interval
		$sql = "((({$this->getEndDateWithin($params['date_end'])})";
		// or start date within interval
		$sql .= " OR ({$this->getStartDateWithin($params['date_start'])})";
		// or both start and end within interval
		$sql .= " OR ({$this->getBothDatesWithin($params['date_start'], $params['date_end'])}))";
		// and the same price
		$sql .= " AND price = {$params['price']})";
		$sql .= " OR ";
		$sql .= "((({$this->getEndDateWithin($params['date_end'])})";
		$sql .= " OR ({$this->getStartDateWithin($params['date_start'])})";
		$sql .= " OR ({$this->getBothDatesWithin($params['date_start'], $params['date_end'])}))";
		$sql .= " AND price <> {$params['price']}";
		// but days do not intersect
		$sql .= " AND mon <> {$params['mon']} AND tue <> {$params['tue']} AND wed <> {$params['wed']}";
		$sql .= " AND thu <> {$params['thu']} AND fri <> {$params['fri']}  AND sat <> {$params['sat']} AND sun <> {$params['sun']})";
		
		$sql = "SELECT * FROM {$this->table} WHERE {$sql}";
		
		$res = $this->query($sql);
		
		return $this->getResponse($res);
	}
	
	/**
	 * Check if current interval has no conflicts with any existing
	 * simple check if interval overlaps with other that has different price but the same days of week
	 * 
	 * @param array $params
	 * @return boolean
	 */
	public function checkInvalid(Array $params): bool {
		// end date within interval
		$sql = "(({$this->getEndDateWithin($params['date_end'])})";
		// or start date within interval
		$sql .= " OR ({$this->getStartDateWithin($params['date_start'])})";
		// or both start and end within interval
		$sql .= " OR ({$this->getBothDatesWithin($params['date_start'], $params['date_end'])}))";
		// and the same price
		$sql .= " AND price <> {$params['price']}";
		// at least one day should overlap to mark interval as not valid
		$sql .= " AND (mon = {$params['mon']} OR tue = {$params['tue']} OR wed = {$params['wed']}";
		$sql .= " OR thu = {$params['thu']} OR fri = {$params['fri']}  OR sat = {$params['sat']} OR sun = {$params['sun']})";
		
		$sql = "SELECT * FROM {$this->table} WHERE {$sql}";
		
		$res = $this->query($sql);
		
		return !!$this->getResponse($res);
	}
	
	private function getEndDateWithin($dateEnd) {
		return "date_end >= '{$dateEnd}' AND date_start <= '{$dateEnd}'";
	}
	
	private function getStartDateWithin($dateStart) {
		return "date_start <= '{$dateStart}' AND date_end >= '{$dateStart}'";
	}
	
	private function getBothDatesWithin($dateStart, $dateEnd) {
		return "date_start >= '{$dateStart}' AND date_end <= '{$dateEnd}'";
	}
	
}