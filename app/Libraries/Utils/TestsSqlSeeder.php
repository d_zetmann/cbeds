<?php

namespace Libraries\Utils;

use Libraries\Utils\DB;
use Exceptions\DBException;

/**
 * Simply runs all SQL queries for test database
 */
class TestsSqlSeeder {
	
	private $queries = [];
	private $connection;
	
	/**
	 * Receives filename. Splits it to separate queries.
	 * Settings DB connection.
	 * @param string $filename
	 */
	public function __construct($filename) {
		$sql = file_get_contents(file($filename));
		$this->queries = array_filter(explode(';', $sql));
		$this->connection = DB::getInstance()->getTestConnection();
	}
	
	/**
	 * Facade method that does the thing: running all queries
	 */
	public function run() {
		foreach ($this->queries as $q) {
			$this->rawQuery($q);
		}
		$count = count($this->queries);
		print(" *** Run {$count} queries *** \n\n");
	}
	
	/**
	 * Running single SQL query
	 * 
	 * @param string $query
	 * @return boolean
	 * @throws DBException
	 */
	private function rawQuery($query) {
		$res = $this->connection->query($query);

		if (!$res) {
			throw new DBException(mysqli_error($this->connection));
		}
		
		return $res;
	}
}
