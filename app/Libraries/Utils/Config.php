<?php

namespace Libraries\Utils;

/**
 * Simple configuration class
 * basing on Registry pattern
 *
 * @author zetmann
 */
class Config {
	private static $properties = [];
	
	public static function get($key) {
		if (isset(self::$properties[$key])) {
			return self::$properties[$key];
		}
		
		return null;
	}
	
	public static function set($key, $val) {
		if (isset(self::$properties[$key])) {
			throw new Exception("Configuration item was already set");
		}
		
		self::$properties[$key] = $val;
	}
}
