<?php

namespace Libraries\Utils;

use Libraries\Utils\Config;

/**
 * Description of DB
 *
 * @author dpog
 */
class DB {
	
	private static $instance;
	
	protected $connection;
	protected $testConnection;
	
	public static function getInstance() {
		if (self::$instance) {
			self::$instance;
		}
		
		return self::$instance = new self;
	}
	
	private function __construct() {
		$dbhost = Config::get('DBHOST');
		$dbname = Config::get('DBNAME');
		$dbuser = Config::get('DBUSER');
		$dbpass = Config::get('DBPASS');
		
		$dbnameTest = Config::get('DBNAME_TEST');
		
		$this->connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
		
		$this->testConnection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbnameTest);
	}
	
	private function __clone() {}
	
	public function getConnection() {
		return $this->connection;
	}
	
	public function getTestConnection() {
		return $this->testConnection;
	}
}
