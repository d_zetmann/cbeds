<?php

namespace Libraries\Interval;

/**
 * Description of Processor
 *
 * @author dpog
 */
class Processor {
	// we can merge if price equals and date overlaps
	private $formatted = [];
	
	private $mergeRules = [
		'date_start' => 'min',
		'date_end' => 'max',
		'price' => 'max',
		'mon' => 'max',
		'tue' => 'max',
		'wed' => 'max',
		'thu' => 'max',
		'fri' => 'max',
		'sat' => 'max',
		'sun' => 'max',
	];

//	public function __construct(Array $intervals) {
//		$this->raw = $intervals;
//	}
	
	public function format() {
		$intervals = array_map(function($int){
			return ['date_start' => $int['date_start'], 'date_end' => $int['date_end']];
		}, $this->raw);
		dd($intervals);
	}
	
	public function isCross($singleInterval, Array $otherIntervals) {
		
	}
	
	public function toMerge($int, $ints) {
		
	}
	
	public function mergeInterval($int1, $int2) {
		$res = [];
		
		foreach($this->mergeRules as $field => $func) {
			if (array_key_exists($field, $int1) && array_key_exists($field, $int1) && function_exists($func)) {
				$res[$field] = call_user_func_array($func, [$int1[$field], $int2[$field]]);
			}
		}
		
		return $res;
	}
	
	/**
	 * Check if two interval overlap
	 * Based on various comparison of start dates and end dates of intervals
	 * 
	 * @param array $int1
	 * @param array $int2
	 * @return boolean
	 */
	public function overlap($int1, $int2) {
		return ($int1['date_end'] >= $int2['date_start'] && $int1['date_start'] <= $int2['date_end']) ||
				($int2['date_end'] >= $int1['date_start'] && $int2['date_end'] <= $int1['date_end']);
	}
	
	public function equalDays($int1, $int2) {
		
	}
}
