<?php

namespace Exceptions;

/**
 * Description of NotFoundException
 *
 * @author zetmann
 */
class NotFoundException extends \Exception {
	
	protected $message = "Resource not found";
	protected $code = 404;

	public function __construct(string $message = "", int $code = 0, \Throwable $previous = null) {
		parent::__construct($this->message, $this->code, $previous);
	}
}
