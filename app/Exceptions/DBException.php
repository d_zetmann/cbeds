<?php

namespace Exceptions;

/**
 * Description of NotFoundException
 *
 * @author zetmann
 */
class DBException extends \Exception {
	
	protected $message = "DB error occured";
	protected $code = 500;

	public function __construct(string $message = "", int $code = 0, \Throwable $previous = null) {
		$msg = $message ?: $this->message;
		parent::__construct($msg, $this->code, $previous);
	}
}
