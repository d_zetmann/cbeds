<?php

namespace Exceptions;

/**
 * Description of ConfigurationException
 *
 * @author zetmann
 */
class ConfigurationException extends \Exception {
	
	protected $message = "Configuration failed";
	protected $code = 500;

	public function __construct(string $message = "", int $code = 0, \Throwable $previous = null) {
		parent::__construct($this->message, $this->code, $previous);
	}
}
