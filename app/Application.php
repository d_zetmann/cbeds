<?php

use Http\IRouter;
use Exceptions\NotFoundException;
use Http\Request;
use Http\Response;
use Libraries\Utils\Config;

/**
 * Description of Application
 *
 * @author zetmann
 */
class Application {

	private $router;
	private $request;

	public function __construct(IRouter $router) {
		$this->router = $router;
		$this->request = new Request();
	}

	public function run(){
		try {
			$route = $this->router->getRoute();
			
			if (class_exists($route['class']) && method_exists($route['class'], $route['method'])) {
				$ctrl = new $route['class']($this->request);				
				return call_user_func([$ctrl, $route['method']]);
			}

			throw new \Exception('');
			
		} catch(NotFoundException $ex){
			return Response::html(Config::get('TEMPLATE_404'), 404);
		} catch(\Exception $ex){
			// Log here
			if (getenv('ENV') === 'PRODUCTION') {
				return Response::html(Config::get('TEMPLATE_500'), 500);
			}
			
			dd($ex->getMessage());
		}
	}
		
}
