<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Libraries\Repository\IntervalRepo;

class IntervalRepoTest extends TestCase {
	
	public function provider_checkInvalid() {
		return [
			[
				// no itntersection -> valid
				['date_start' => '2018-05-05', 'date_end' => '2018-05-25', 'price' => 5, 'mon' => '1', 'tue' => '1', 'wed' => '1', 'thu' => '0', 'fri' => '1', 'sat' => '0', 'sun' => '0'],
				false
			],
			[
				// intersects but the same price -> valid
				['date_start' => '2018-04-05', 'date_end' => '2018-04-15', 'price' => 99, 'mon' => '1', 'tue' => '1', 'wed' => '1', 'thu' => '1', 'fri' => '1', 'sat' => '1', 'sun' => '1'],
				false
			],
			[
				// intersects, different price but different days, so -> valid
				['date_start' => '2018-04-05', 'date_end' => '2018-04-15', 'price' => 5, 'mon' => '0', 'tue' => '1', 'wed' => '0', 'thu' => '1', 'fri' => '0', 'sat' => '0', 'sun' => '0'],
				false
			],
			[
				// intersects, different price, days also intersect, so -> invalid
				['date_start' => '2018-04-05', 'date_end' => '2018-04-15', 'price' => 112, 'mon' => '4', 'tue' => '1', 'wed' => '1', 'thu' => '1', 'fri' => '1', 'sat' => '1', 'sun' => '1'],
				true
			],
			[
				// intersects and fully covers interval, different price, days also intersect, so -> invalid
				['date_start' => '2018-04-05', 'date_end' => '2018-05-01', 'price' => 112, 'mon' => '4', 'tue' => '1', 'wed' => '1', 'thu' => '1', 'fri' => '1', 'sat' => '1', 'sun' => '1'],
				true
			],
			[
				// intersects and fully covers interval, same price -> valid
				['date_start' => '2018-04-05', 'date_end' => '2018-05-01', 'price' => 99, 'mon' => '1', 'tue' => '1', 'wed' => '1', 'thu' => '1', 'fri' => '1', 'sat' => '1', 'sun' => '1'],
				false
			],
		];
	}
	
	public function provider_fetchIntersecting() {
		return [
			[
				// no itntersection -> valid
				['date_start' => '2018-01-05', 'date_end' => '2018-01-28', 'price' => 5, 'mon' => '1', 'tue' => '1', 'wed' => '1', 'thu' => '0', 'fri' => '1', 'sat' => '0', 'sun' => '0'],
				0
			],
			[
				// intersects with one interval
				['date_start' => '2018-02-01', 'date_end' => '2018-02-28', 'price' => 55, 'mon' => '1', 'tue' => '0', 'wed' => '1', 'thu' => '0', 'fri' => '1', 'sat' => '0', 'sun' => '1'],
				1
			],
			[
				// intersects with more than one
				['date_start' => '2018-01-04', 'date_end' => '2018-01-22', 'price' => 105, 'mon' => '1', 'tue' => '1', 'wed' => '1', 'thu' => '1', 'fri' => '1', 'sat' => '1', 'sun' => '1'],
				3
			],
		];
	}
	
	/**
	 * @dataProvider provider_checkInvalid
	 */
	public function test_checkInvalid($int, $res){
		$repo = new IntervalRepo();
		
		$this->assertEquals($res, $repo->checkInvalid($int));
	}
	
	/**
	 * @dataProvider provider_fetchIntersecting
	 */
	public function test_fetchIntersecting($int, $count){
		$repo = new IntervalRepo();
		
		$this->assertEquals($count, count($repo->fetchIntersecting($int)));
	}
}

