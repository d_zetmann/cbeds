<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Libraries\Interval\Processor;
/**
 * Description of ProcessTest
 *
 * @author dpog
 */
class ProcessTest extends TestCase {
	
	/**
	 * This is interval provider
	 * to compare and check if they are overlapping
	 * in comments there are dashes that reflect intervals graphically
	 */
	public function provider_overlap(){
		return [
			[
				['date_start' => '2018-04-24', 'date_end' => '2018-04-27'],//   ----
				['date_start' => '2018-04-28', 'date_end' => '2018-04-29'],//        --
				false
			],
			[
				['date_start' => '2018-04-24', 'date_end' => '2018-04-27'],//   ----
				['date_start' => '2018-04-25', 'date_end' => '2018-04-29'],//    -----
				true
			],
			[
				['date_start' => '2018-04-24', 'date_end' => '2018-04-27'],//   ----
				['date_start' => '2018-04-22', 'date_end' => '2018-04-26'],// -----
				true
			],
			[
				['date_start' => '2018-04-24', 'date_end' => '2018-04-27'],//     ----
				['date_start' => '2018-04-21', 'date_end' => '2018-04-23'],//  ---
				false
			],
			[
				['date_start' => '2018-04-22', 'date_end' => '2018-04-27'],//   ------
				['date_start' => '2018-04-24', 'date_end' => '2018-04-26'],//    ---
				true
			],
			[
				['date_start' => '2018-04-24', 'date_end' => '2018-04-26'],//    ---
				['date_start' => '2018-04-22', 'date_end' => '2018-04-27'],//   ------
				true
			],
		];
	}
	
	public function provider_mergeInterval() {
		return [
			[
				['date_start' => '2018-04-24', 'date_end' => '2018-04-27', 'price' => 5, 'mon' => '1', 'tue' => '1', 'wed' => '1', 'thu' => '0', 'fri' => '1', 'sat' => '0', 'sun' => '0'],
				['date_start' => '2018-04-25', 'date_end' => '2018-04-28', 'price' => 5, 'mon' => '1', 'tue' => '0', 'wed' => '1', 'thu' => '0', 'fri' => '1', 'sat' => '0', 'sun' => '1'],
				['date_start' => '2018-04-24', 'date_end' => '2018-04-28', 'price' => 5, 'mon' => '1', 'tue' => '1', 'wed' => '1', 'thu' => '0', 'fri' => '1', 'sat' => '0', 'sun' => '1']
			]
		];
	}
	
	/**
	 * @dataProvider provider_overlap
	 */
	public function test_overlap($int1, $int2, $res){
		$processor = new Processor();
		$this->assertEquals($res, $processor->overlap($int1, $int2));
	}
	
	/**
	 * 
	 * @dataProvider provider_mergeInterval
	 */
	public function test_mergeInterval($int1, $int2, $res) {
		
		$processor = new Processor();
		$merged = $processor->mergeInterval($int1, $int2);
		
		$this->assertEquals($res, $merged);
	}
}
