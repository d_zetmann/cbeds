<?php
// test are run by command in root dir: ./vendor/bin/phpunit --bootstrap=./tests/bootstrap.php ./tests
ini_set('display_errors', 'on');
error_reporting(E_ALL);

function dd($var) {
	var_dump($var);
	die;
}

require __DIR__ . '/../vendor/autoload.php';

print(" *** Running pre test setup *** \n\n");

// set config
\Libraries\Utils\Config::set('DBHOST', 'localhost');
\Libraries\Utils\Config::set('DBNAME', 'cbeds');
\Libraries\Utils\Config::set('DBUSER', 'root');
\Libraries\Utils\Config::set('DBPASS', 'root');
\Libraries\Utils\Config::set('DBNAME_TEST', 'cbeds_test');

putenv("ENV=TESTING");

\Libraries\Utils\DB::getInstance();

$test = new \Libraries\Utils\TestsSqlSeeder(__DIR__ . DIRECTORY_SEPARATOR . 'seeders.sql');

print(" *** Filling test database *** \n\n");
$test->run();

print(" *** Database filled successfully *** \n\n");