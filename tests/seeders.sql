DROP TABLE IF EXISTS `intervals`;

CREATE TABLE `intervals` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `date_start` date NOT NULL,
 `date_end` date NOT NULL,
 `price` float NOT NULL,
 `mon` tinyint(1) NOT NULL,
 `tue` tinyint(1) NOT NULL,
 `wed` tinyint(1) NOT NULL,
 `thu` tinyint(1) NOT NULL,
 `fri` tinyint(1) NOT NULL,
 `sat` tinyint(1) NOT NULL,
 `sun` tinyint(1) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `intervals` 
(`id`, `date_start`, `date_end`, `price`, `mon`, `tue`, `wed`, `thu`, `fri`, `sat`, `sun`) 
VALUES
(1,	   '2018-02-02', '2018-03-02',  55,     1,     0,     1,     0,     1,     0,     1),
(2,    '2018-04-10', '2018-04-30',  99,     1,     0,     1,     0,     1,     1,     1),
(4,    '2018-06-01', '2018-08-31',  120,    1,     1,     1,     1,     1,     1,     1),
(5,    '2018-09-01', '2018-11-30',  99,     1,     1,     1,     1,     1,     0,     0),
(6,    '2018-09-01', '2018-11-30',  105,    0,     0,     0,     0,     0,     1,     1),
(7,    '2018-01-01', '2018-01-05',  105,    1,     1,     1,     1,     1,     1,     1),
(8,    '2018-01-07', '2018-01-15',  105,    1,     1,     1,     1,     1,     1,     1),
(9,    '2018-01-20', '2018-01-25',  105,    1,     1,     1,     1,     1,     1,     1);